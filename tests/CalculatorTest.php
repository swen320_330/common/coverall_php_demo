<?php
use PHPUnit\Framework\TestCase;

require dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . "html" . DIRECTORY_SEPARATOR . "phpinfo.php";

class CalculatorTest extends TestCase
{
    public function testCal(): void
    {
        $this->assertEquals(
            110,
            Calculator::calculate(10)
        );
    }

}